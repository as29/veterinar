package main;
import java.util.Scanner;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mysql.fabric.xmlrpc.base.Data;

import model.Animal;
import model.Doctor;
import util.CommitListener;
import util.DatabaseUtil;
import model.Programare;
public class Main {

	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.addListener(new CommitListener() {
			
			@Override
			public boolean commited() {
				System.out.println("tranzactie facuta");
				return true;
				
			}
		});
		Scanner in = new Scanner(System.in);
		dbUtil.setUp();
		System.out.println("0.iesire");
		System.out.println("1.add animal");
		System.out.println("2.add doctor");
		System.out.println("3.add programare");
		System.out.println("4.delete animal");
		System.out.println("5.delete doctor");
		System.out.println("6.delete programare");
		System.out.println("7.afisare animale");
		System.out.println("8.afisare doctori");
		System.out.println("9.afisare programari");
		System.out.println("10.update nume animal");
		System.out.println("11.update nume doctor");
		System.out.println("12.update data programare");
		System.out.println("13.afisare animale dupa nume");
		System.out.println("14.afisare doctori dupa nume");
		int raspuns = 1;
		int idanimal;
		int iddoctor;
		int idprogramare;
		String nume;
		while(raspuns!=0) {
			dbUtil.startTransaction();
			raspuns=in.nextInt();
			switch(raspuns) {
			case 1:
				nume=in.next();
				dbUtil.saveAnimal(nume);
			break;
			case 2:
				nume=in.next();
				dbUtil.saveDoctor(nume);
				
			break;
			case 3:

				idanimal=in.nextInt();
				iddoctor=in.nextInt();
				Date data=new Date(in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt());
				dbUtil.saveprogramare(dbUtil.findAnimal(idanimal), dbUtil.findDoctor(iddoctor), data);
			break;
			case 4:
				idanimal=in.nextInt();
				dbUtil.deleteAnimal(dbUtil.findAnimal(idanimal));
			break;
			
			case 5:
				iddoctor=in.nextInt();
				dbUtil.deleteDoctor(dbUtil.findDoctor(iddoctor));
			break;
			case 6:
				idprogramare=in.nextInt();
				dbUtil.deleteProgramare(dbUtil.findProgramare(idprogramare));
			break;
			case 7:
				dbUtil.printAllAnimals();
			break;
			case 8:
				dbUtil.printAllDoctors();
			break;
			case 9:
				dbUtil.printProgramari(dbUtil.listProgramari());
			break;
			case 10:
				idanimal=in.nextInt();
				nume=in.next();
				dbUtil.updateAnimal(idanimal, nume);
			break;
			case 11:
				iddoctor=in.nextInt();
				nume=in.next();
				dbUtil.updateDoctor(iddoctor, nume);
			break;
			case 12:
				idprogramare=in.nextInt();
				Date data1=new Date(in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt(), in.nextInt());
				dbUtil.updateProgramare(idprogramare, data1);
			break;
			case 13:
				dbUtil.listAnimal(in.next());
			break;
			case 14:
				dbUtil.listDoctor(in.next());
			break;
			}
			dbUtil.commitTransaction();
			
		}
		dbUtil.closeEntityManager();
		in.close();
	}

}
