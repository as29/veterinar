package util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.management.ListenerNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Animal;
import model.Doctor;
import model.Programare;
public class DatabaseUtil {
	public List<CommitListener> listeners =new ArrayList<CommitListener>();
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() throws Exception {
		entityManagerFactory=Persistence.createEntityManagerFactory("VeterinarJPAv1");
		entityManager=entityManagerFactory.createEntityManager();
	}
	public void addListener(CommitListener listener) {
		listeners.add(listener);
	}
	/**
	 * 
	 * @param animal
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
		
	}
	/**
	 * 
	 * @param nume
	 */
	public void saveAnimal(String nume) {
		Animal newAnimal=new Animal();
		newAnimal.setName(nume);
		entityManager.persist(newAnimal);
		
	}

		/**
		 * 
		 * @param idanimal
		 * @return animalul cu acel id
		 */
	public Animal findAnimal(int idanimal) {
		Animal animal=(Animal) entityManager.createNativeQuery("select * from veterinar.animal WHERE idanimal="+idanimal,Animal.class).getSingleResult();
		return animal;
	}
	/**
	 * 
	 * @param name
	 * afiseaza animalele cu nume
	 */
	public void listAnimal(String name){
		List<Animal> animale= entityManager.createNativeQuery("Select * from veterinar.animal WHERE name='"+name+"'",Animal.class)
				.getResultList();
		printAnimals(animale);
	}
	/**
	 * afiseaza toate animalele
	 */
	public void printAllAnimals() {
		List<Animal>results=entityManager.createNativeQuery("Select * from veterinar.animal",Animal.class)
				.getResultList();
		printAnimals(results);
	}
	public HashMap<Integer,Animal> getAnimals(){
		HashMap<Integer,Animal> animals=new HashMap<>();
		List<Animal>results=entityManager.createNativeQuery("Select * from veterinar.animal",Animal.class)
				.getResultList();
		for(Animal animal:results)
		{
			animals.put(animal.getIdanimal(),animal);
		}
		return animals;
	}
	/**
	 * 
	 * @param animals
	 * afiseaza lista cu animale
	 */
	public void printAnimals(List<Animal> animals) {
		for(Animal animal : animals) {
			System.out.println(animal.toString());
		}
	}
	/**
	 * 
	 * @param idanimal
	 * @param nume
	 * modifica numele unui animal
	 */
	public void updateAnimal(int idanimal,String nume)
	{
		Animal animal = entityManager.find(Animal.class, idanimal);
		animal.setName(nume);
	}
	/**
	 * 
	 * @param animal
	 * sterge un animal
	 */
	public void deleteAnimal( Animal animal) {
		entityManager.remove(animal);
	}
	
	
	
	/**
	 * 
	 * @param doctor
	 * adauga un doctor
	 */
	public void saveDoctor(Animal doctor) {
		entityManager.persist(doctor);
	}
	/**
	 * 
	 * @param nume
	 * creaza si adauga un doctor cu nume
	 */
	public void saveDoctor(String nume) {
		Doctor newDoctor=new Doctor();
		newDoctor.setNume(nume);
		entityManager.persist(newDoctor);
	}
	/**
	 * 
	 * @param iddoctor
	 * @return doctorul cu id
	 */
	public Doctor findDoctor(int iddoctor) {
		Doctor doctor=(Doctor) entityManager.createNativeQuery("select * from veterinar.doctor WHERE iddoctor="+iddoctor,Doctor.class).getSingleResult();
		return doctor;
	}
	/**
	 * 
	 * @param name
	 * afiseaza doctori cu nume
	 */
	public void listDoctor(String name){
		List<Doctor> doctori= entityManager.createNativeQuery("Select * from veterinar.doctor WHERE name='"+name+"'",Doctor.class)
				.getResultList();
		printDoctori(doctori);
	}
	/**
	 * afiseaza toti doctorii
	 */
	public void printAllDoctors() {
		List<Doctor>results=entityManager.createNativeQuery("Select * from veterinar.doctor",Doctor.class)
				.getResultList();
		printDoctori(results);
	}
	public HashMap<Integer,Doctor> getDoctors(){
		HashMap<Integer,Doctor> doctori=new HashMap<>();
		List<Doctor>results=entityManager.createNativeQuery("Select * from veterinar.doctor",Doctor.class)
				.getResultList();
		for(Doctor doctor:results)
		{
			doctori.put(doctor.getIddoctor(),doctor);
		}
		return doctori;
	}
	/**
	 * 
	 * @param doctori
	 * afiseaza cu doctori
	 */
	public void printDoctori(List<Doctor> doctori) {
		for(Doctor doctor : doctori) {
			System.out.println("ID Doctor: " + doctor.getIddoctor()+" nume: " + doctor.getNume());
		}
	}
	/**
	 * 
	 * @param iddoctor
	 * @param nume
	 * schimba numele unui doctor
	 */
	public void updateDoctor(int iddoctor,String nume)
	{
		Doctor doctor = entityManager.find(Doctor.class, iddoctor);
		doctor.setNume(nume);
	}
	/**
	 * 
	 * @param doctor
	 * sterge un doctor
	 */
	public void deleteDoctor( Doctor doctor) {
		entityManager.remove(doctor);
	}
	
	/**
	 * 
	 * @param animal
	 * @param doctor
	 * @param data
	 * adauga o programare
	 * 
	 */
	public void saveprogramare(Animal animal,Doctor doctor,Date data) {
		Programare newProgramare=new Programare();
		newProgramare.setData(data);
		newProgramare.setAnimal(animal);
		newProgramare.setDoctor(doctor);
		entityManager.persist(newProgramare);
	}
	/**
	 * 
	 * @param programare
	 * adauga o programare
	 */
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
		
	}
	/**
	 * 
	 * @param idprogramare
	 * @return returneaza programare cu id
	 */
	public Programare findProgramare(int idprogramare) {
		Programare programare=(Programare) entityManager.createNativeQuery("SELECT * FROM veterinar.programare WHERE idprogramare="+idprogramare,Programare.class).getSingleResult();
		return programare;
	}
	/**
	 * 
	 * @return returneaza toate programarile ordonate
	 */
	public List<Programare> listProgramari(){
		List<Programare>programari=entityManager.createNativeQuery("SELECT * FROM veterinar.programare ORDER BY	data",Programare.class)
				.getResultList();
		Collections.sort(programari, (employee1, employee2) -> employee1.getData().compareTo(employee2.getData()));
		return programari;
		
	}
	/**
	 * 
	 * @param programari
	 * afiseaza lista cu programari
	 */
	public void printProgramari(List<Programare>programari) {
		for (Programare programare:programari)
		{
			System.out.println(programare.toString());
		}
	}
	/**
	 * 
	 * @param idprogramare
	 * @param data
	 * modifica data unei programari
	 */
	public void updateProgramare(int idprogramare,Date data)
	{
		Programare programare = entityManager.find(Programare.class, idprogramare);
		programare.setData(data);
	}
	/**
	 * 
	 * @param programare
	 * sterge o programare
	 */
	public void deleteProgramare( Programare programare) {
		entityManager.remove(programare);
	}

	public void startTransaction()
	{
		entityManager.getTransaction().begin();
	}
	public void commitTransaction() {
		entityManager.getTransaction().commit();
		for(CommitListener listener : listeners)
		{
			listener.commited();
		}
	}
	public void closeEntityManager() {
		entityManager.close();
	}

}
