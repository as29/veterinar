package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idanimal;

	private String name;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="animal",cascade=CascadeType.REMOVE)
	private List<Programare> programari;

	public Animal() {
	}

	public int getIdanimal() {
		return this.idanimal;
	}

	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Programare> getProgramari() {
		return this.programari;
	}

	public void setProgramares(List<Programare> programares) {
		this.programari = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramari().add(programare);
		programare.setAnimal(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramari().remove(programare);
		programare.setAnimal(null);

		return programare;
	}

	@Override
	public String toString() {
		return " idanimal=" + idanimal + " " + name;
		
	}

}