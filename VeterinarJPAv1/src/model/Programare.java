package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	//bi-directional many-to-one association to Animal
	@ManyToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="idanimal")
	private Animal animal;

	//bi-directional many-to-one association to Doctor
	@ManyToOne
	@JoinColumn(name="iddoctor")
	private Doctor doctor;

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Doctor getDoctor() {
		return this.doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	@Override
	public String toString() {
		return "idprogramare=" + idprogramare + ", " + data + " " + animal + " "
				+ doctor;
	}

}