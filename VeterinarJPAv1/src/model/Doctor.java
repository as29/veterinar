package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the doctor database table.
 * 
 */
@Entity
@NamedQuery(name="Doctor.findAll", query="SELECT d FROM Doctor d")
public class Doctor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int iddoctor;

	private String nume;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="doctor")
	private List<Programare> programares;

	public Doctor() {
	}

	public int getIddoctor() {
		return this.iddoctor;
	}

	public void setIddoctor(int iddoctor) {
		this.iddoctor = iddoctor;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setDoctor(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setDoctor(null);

		return programare;
	}

	@Override
	public String toString() {
		return "iddoctor=" + iddoctor + " " + nume;
	}

}